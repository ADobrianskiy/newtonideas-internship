var gulp = require('gulp')
var sass = require('gulp-ruby-sass');

gulp.task('sass', function() {
    return sass('./views/scss/example.scss', {
            style: 'expanded'
        })
        .pipe(gulp.dest('./public/css'));
});

gulp.task('watch', function() {
    gulp.watch('./views/scss/**/*.scss', ['sass']);
});

// Default Task
gulp.task('default', ['sass', 'watch']);
