/**
 * Created by Anna on 30.09.2015.
 */
$(document).ready(function(){
    $("#addButt").on('click' , function(){
        var name = $("#Name").val();
        if(!isValidString(name)){
            alert("not valid name");
            return;
        }
        var surname = $("#Surname").val();
        if(!isValidString(surname)){
            alert("not valid surname");
            return;
        }
        var salary = Number($("#Salary").val());
        if(!isValidSalary(salary)){
            alert("not valid salary");
            return;
        }
        var templ = $("#template").clone();
        templ.css("display", "block");
        templ.children(".name").text(name);
        templ.children(".surname").text(surname);
        templ.children(".salary").text(salary);
        templ.insertAfter( $( "#template" ) );
        $("#Name").val("");
        $("#Surname").val("");

        $("#Salary").val("");



    })
}); //������ �� �����������!!!

function isValidSalary (number){
    var res = true;
    if (!number || number < 0 || number > 10000){
        res = false;
    }
    return res;
}

function isValidString (string){
    var res = true;
    if (!string || string.indexOf(" ") > -1 || !isUpperCase(string[0])){
        res = false;
    }
    return res;
}
    function isUpperCase(str) {
        return str === str.toUpperCase();
    }