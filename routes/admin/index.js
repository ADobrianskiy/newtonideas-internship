var express = require('express');
var index = express.Router();

/* GET users listing. */
index.get('/', function(req, res, next) {
  res.render('admin/view', {
    title: "Admin Panel",
    body_view: "body" //path to your view from views/admin/
  });
});

/* Here we are adding all routes for component */
module.exports = function(app){
  app.use('/', index);
  //app.use('/component', router2);
};
