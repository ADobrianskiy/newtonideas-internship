var express = require('express');
var index = express.Router();

/* GET users listing. */
index.get('/', function(req, res, next) {
  res.render('component/view', {
    title: "Component",
    body_view: "body" //path to your view from views/component/
  });
});

/* Here we are adding all routes for admin console */
module.exports = function(app){
  app.use('/admin', index);
  //app.use('/admin2', router2);
};
